//on pointe sur les input de type radio
let put1 = document.getElementById('tva9');
let put2 = document.getElementById('tva11');
let put3 = document.getElementById('tva19');


//on pointe sur le Quantite  de produit
let quantite = document.getElementById('quantite');
// on pointe sur le prix unitaire du prduit
let prix = document.getElementById('prix-HT');
//on pointe sur le montant total sans tva
let montant = document.getElementById('montant');
//om pointe sur le montant total avec tva
let total = document.getElementById('total');
//on pointe sur le button de reinitialisation formulaire
let effacer = document.getElementById('effacer');
// on pointe sur le button de calcul.
let calcul = document.getElementById('calcul');

// les variable globales

let tva ='';




// les avenements sur button calculer
calcul.addEventListener('click',calculer);
//les avenements pour effacer tout 
effacer.addEventListener('click',efface);


// on choisit un prodiut il nous renvoie le prix

function affiche(){
    // on pointe sur le produit choisis
    let produit = document.getElementById('selection').value;
    switch (produit) {
        case "ordinateur":
            prix.value = 2500;
            break;
        case "imprimante":
            prix.value = 3500;
            break;
        case "souris":
            prix.value = 5500;
            break;
        default:
            prix.value = 4500;
            break;
    }
}

// on ecrit les fonctions qui verifient les donnéées entrant
function calculer(){
    let m = prix.value;
    let n = parseInt(quantite.value);

    //on choisit le taux de tva
    if(put1.checked){
        tva =0.09;
    }
    else if(put2.checked){
        tva = 0.11;
    }
    else{
        tva = 0.19;
    }

    let q = m*n;
    montant.value=q;
    let d= q*tva;
    total.value =q + d;
}

function efface(){
    let formulaire = document.getElementById('form');
    formulaire.reset();
}